import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import tensorflow.examples.tutorials.mnist.input_data as input_data
mnist = input_data.read_data_sets("fashion_mnist/", one_hot = True)

def weight(shape):
    return tf.Variable(tf.truncated_normal(shape,stddev=0.1),name='w')
def bias(shape):
    return tf.Variable(tf.constant(0.1,shape=shape),name='b')
def conv2d(x,w):
    return tf.nn.conv2d(x,w,strides=[1,1,1,1],padding='SAME')
def max_pool_2x2(x):
    return tf.nn.max_pool(x,ksize=[1,2,2,1],strides=[1,2,2,1],padding='SAME')
with tf.name_scope('input_layer'):
    x=tf.placeholder("float",shape=[None,784],name='x')
    x_image=tf.reshape(x,[-1,28,28,1])
with tf.name_scope('C1_Conv'):
    w1=weight([3,3,1,16])
    b1=bias([16])
    Conv1=conv2d(x_image,w1)+b1
    C1_Conv=tf.nn.relu(Conv1)
with tf.name_scope('C1_Pool'):
    C1_Pool=max_pool_2x2(C1_Conv)
with tf.name_scope('C2_Conv'):
    w2=weight([3,3,16,36])
    b2=bias([36])
    Conv2=conv2d(C1_Pool,w2)+b2
    C2_Conv=tf.nn.relu(Conv2)
with tf.name_scope('C2_Pool'):
    C2_Pool=max_pool_2x2(C2_Conv)
with tf.name_scope('D_Flat'):
    D_Flat=tf.reshape(C2_Pool,[-1,1764])
with tf.name_scope('D_Hidden_Layer'):
    w3=weight([1764,1024])
    b3=bias([1024])
    D_Hidden=tf.nn.relu(tf.matmul(D_Flat,w3)+b3)
    D_Hidden_Dropout=tf.nn.dropout(D_Hidden,keep_prob=0.8)
with tf.name_scope('Output_Layer'):
    w4=weight([1024,10])
    b4=bias([10])
    y_predict=tf.nn.softmax(tf.matmul(D_Hidden_Dropout,w4)+b4,name='y_predict')
with tf.name_scope('optimizer'):
    y_label=tf.placeholder("float",[None,10],name='y_label')
    loss_function=tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_predict,labels=y_label))
    global_step = tf.Variable(0, trainable=False)
    learning_rate=tf.train.exponential_decay(0.001, global_step, 550, 0.7)
    optimizer=tf.train.AdamOptimizer(learning_rate).minimize(loss_function)
with tf.name_scope('evaluate_model'):
    correct_prediction=tf.equal(tf.argmax(y_label,1),tf.argmax(y_predict,1))
    accuracy=tf.reduce_mean(tf.cast(correct_prediction,"float"))
saver = tf.train.Saver()
sess=tf.Session()
sess.run(tf.global_variables_initializer())
saver.restore(sess, "./model/fashion_mnist")
graph = tf.get_default_graph()
prediction_result=sess.run(tf.argmax(y_predict,1),feed_dict={x:mnist.test.images,y_label:mnist.test.labels})
#prediction_result[:10]
def plot_image_labels_prediction(images,labels,prediction,idx,num=10):
    fig=plt.gcf()
    fig.set_size_inches(12,14)
    if num>25:
        num=25
    for i in range(0,num):
        ax=plt.subplot(5,5,1+i)
        ax.imshow(np.reshape(images[idx],(28,28)),cmap='binary')
        title="label="+str(np.argmax(labels[idx]))
        if len(prediction)>0:
            title+=",predict="+str(prediction[idx])
        ax.set_title(title,fontsize=10)
        ax.set_xticks([]);ax.set_yticks([])
        idx+=1
    plt.show()
plot_image_labels_prediction(mnist.test.images,mnist.test.labels,prediction_result,80)
